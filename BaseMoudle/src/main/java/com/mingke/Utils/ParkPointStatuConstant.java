package com.mingke.Utils;

public enum ParkPointStatuConstant {
    UNUSe(101, "节点未启用"),
    ONUSe(102, "节点使用中"),
    CANNOTUSe(103, "节点掉线无信号");

    private int code;
    private String msg;

    ParkPointStatuConstant(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
