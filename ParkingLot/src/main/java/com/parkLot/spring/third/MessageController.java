package com.parkLot.spring.third;

import com.mingke.Utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class MessageController {

    @Autowired
    private MessageUtils utils;

    @GetMapping("getMessage")
    public R getMessage(@RequestParam("mobile") String mobile, @RequestParam("code") String code) throws IOException {
        return utils.sendMessageByPost(mobile, code);
    }
}
