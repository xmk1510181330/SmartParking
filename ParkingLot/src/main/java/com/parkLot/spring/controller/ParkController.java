package com.parkLot.spring.controller;

import com.mingke.Domain.ParkEntity;
import com.mingke.TransformObject.LocationTO;
import com.mingke.Utils.PageUtils;
import com.mingke.Utils.R;
import com.parkLot.spring.service.ParkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:40
 */
@RestController
@RequestMapping("/park")
public class ParkController {

    @Autowired
    private ParkService parkService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = parkService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{parkId}")
    public R info(@PathVariable("parkId") Integer parkId){
		ParkEntity park = parkService.getById(parkId);

        return R.ok().put("park", park);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody ParkEntity park){
        park.setAddTime(new Date());
        park.setUpdateTime(new Date());
        park.setDaleted(0);
		parkService.save(park);

        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody ParkEntity park){
        park.setUpdateTime(new Date());
		parkService.updateById(park);

        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Integer[] parkIds){
		parkService.removeByIds(Arrays.asList(parkIds));

        return R.ok();
    }

    @PostMapping("/getParkWithLocation")
    public R getUserLocation(@RequestBody LocationTO location){
        System.out.println("获取到用户定位信息");
        List<ParkEntity> result = parkService.getUserLocation(location);
        return R.ok().put("result", result);
    }

}
