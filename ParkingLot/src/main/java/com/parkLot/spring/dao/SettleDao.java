package com.parkLot.spring.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mingke.Domain.SettleEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:39
 */
@Mapper
public interface SettleDao extends BaseMapper<SettleEntity> {
	
}
