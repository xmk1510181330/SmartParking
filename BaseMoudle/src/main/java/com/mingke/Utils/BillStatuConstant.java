package com.mingke.Utils;

public enum BillStatuConstant {
    UNPAY(101, "订单待支付"),
    FINSHPAY(102, "订单已支付"),
    PARKING(103, "停车中");

    private Integer code;
    private String msg;

    BillStatuConstant(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
