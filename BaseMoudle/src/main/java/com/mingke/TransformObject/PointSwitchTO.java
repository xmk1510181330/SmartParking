package com.mingke.TransformObject;

import lombok.Data;

@Data
public class PointSwitchTO {

    private Integer uid;
    private Integer pointId;
    private Integer switchStatu;
}
