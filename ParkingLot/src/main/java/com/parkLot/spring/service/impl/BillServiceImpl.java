package com.parkLot.spring.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mingke.Domain.*;
import com.mingke.Utils.PageUtils;
import com.mingke.Utils.Query;
import com.mingke.ViewObject.BillInfoViewObject;
import com.parkLot.spring.dao.BillDao;
import com.parkLot.spring.service.BillService;
import com.parkLot.spring.service.ParkPointService;
import com.parkLot.spring.service.ParkService;
import com.parkLot.spring.service.SettleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;




@Service("billService")
public class BillServiceImpl extends ServiceImpl<BillDao, BillEntity> implements BillService {

    @Autowired
    private ParkService parkService;

    @Autowired
    private ParkPointService parkPointService;

    @Autowired
    private SettleService settleService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        //String key = (String) params.get("key");
        QueryWrapper<BillEntity> wrapper = new QueryWrapper<>();
        IPage<BillEntity> page = this.page(
                new Query<BillEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<BillInfoViewObject> getBillListByUid(Integer id) {
        QueryWrapper<BillEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("bill_customer_id", id);
        wrapper.eq("deleted", 0);
        wrapper.orderByAsc("add_time");
        List<BillEntity> list = list(wrapper);
        List<BillInfoViewObject> result = new ArrayList<>();
        List<ParkEntity> parks = parkService.list();
        List<SettleEntity> settles = settleService.list();
        Date now = new Date();
        for(BillEntity bill : list){
            BillInfoViewObject info = new BillInfoViewObject();
            BeanUtils.copyProperties(bill, info);
            Date end = now;
            if(bill.getBillEnd()==null){
                //此时车主还没走，使用系统时间
                String timeStr = buildAliveString(bill.getBillStart(), now);
                info.setTotleTime(timeStr);
            }else {
                //停车完成，使用end
                end = bill.getBillEnd();
                String timeStr = buildAliveString(bill.getBillStart(), bill.getBillEnd());
                info.setTotleTime(timeStr);
            }
            //封装停车场信息
            ParkPointEntity parkPointEntity = parkPointService.getById(bill.getBillParkingId());
            for(ParkEntity parkEntity : parks){
                if(parkPointEntity.getParkId()==parkEntity.getParkId()){
                    info.setBillParkName(parkEntity.getParkName());
                    if(bill.getBillEnd() != null && bill.getBillFee() != 0){
                        //如果end不为空，而且价格已经有数值，则说明之前计算过，可以不再重复计算

                    }else {
                        for(SettleEntity settleEntity : settles){
                            if(settleEntity.getSettleId()==parkEntity.getParkSettleId()){
                                //计费，不足一个小时按一个小时计费
                                Integer hours = buildHoursForBill(bill.getBillStart(), end);
                                if(hours == 0){
                                    hours = hours + 1;
                                }
                                bill.setBillFee(settleEntity.getSettleSmallUnitPrice()*hours);

                                updateById(bill);
                                info.setBillFee(bill.getBillFee());
                            }
                        }
                    }
                }
            }

            result.add(info);
        }
        return result;
    }

    private String buildAliveString(Date start, Date now) {
        long between = now.getTime() - start.getTime();
        long day = between / (24 * 60 * 60 * 1000);
        long hour = (between / (60 * 60 * 1000) - day * 24);
        long min = ((between / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s = (between / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        if(day>7L){
            return "一周前";
        }
        return day + "天" + hour + "小时" + min + "分" + s + "秒";
    }

    private Integer buildHoursForBill(Date start, Date now) {
        long between = now.getTime() - start.getTime();
        long hour = (between / (60 * 60 * 1000));
        long min = ((between / (60 * 1000)) - hour * 60);
        if(min > 30){
            hour = hour + 1;
        }
        return (int)hour;
    }

}