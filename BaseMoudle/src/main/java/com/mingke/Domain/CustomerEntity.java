package com.mingke.Domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:40
 */
@Data
@TableName("smartpark_customer")
public class CustomerEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer customerId;
	/**
	 * 真实姓名
	 */
	private String customerRealname;
	/**
	 * 昵称
	 */
	private String customerNickname;
	/**
	 * 电话号码
	 */
	private String customerPhone;
	/**
	 * 登陆密码
	 */
	private String customerPassword;
	/**
	 * 微信登录open_id
	 */
	private String customerOpenid;
	/**
	 * 头像
	 */
	private String customerPic;
	/**
	 * 会员等级
	 */
	private Integer customerLevel;
	/**
	 * 添加时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Integer deleted;

}
