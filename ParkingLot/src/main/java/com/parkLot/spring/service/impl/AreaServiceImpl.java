package com.parkLot.spring.service.impl;

import com.mingke.Domain.AreaEntity;
import com.mingke.Domain.CustomerEntity;
import com.mingke.Utils.PageUtils;
import com.mingke.Utils.Query;
import com.parkLot.spring.dao.AreaDao;
import com.parkLot.spring.service.AreaService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.util.StringUtils;


@Service("areaService")
public class AreaServiceImpl extends ServiceImpl<AreaDao, AreaEntity> implements AreaService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        //AreaEntity
        String key = (String) params.get("key");
        QueryWrapper<AreaEntity> wrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(key)) {
            wrapper.like("area_name", key);
        }
        IPage<AreaEntity> page = this.page(
                new Query<AreaEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

}