package com.parkLot.spring.third;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.mingke.ViewObject.PayInfoViewObject;
import com.mingke.ViewObject.PayVo;
import lombok.Data;
import org.springframework.stereotype.Component;

/*
 * 实现支付宝的相关操作
 * 最后发现，支付宝支付，如果要拉起支付宝支付的话，还是需要商户号的
 * 所以最后妥协了，还是实现一个网页登陆支付
 */
@Component
@Data
public class AliPayUtils {

    //在支付宝创建的应用的id
    private   String app_id = "2016102500757787";

    // 用户私钥
    public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCznyxmQUn86tg0LgVpYSlY3gKsOtrMmtxL7SfiOfkxnuIoA0lLSXKduwbi2pE5oHvlj9jX/EYs67CHfcTp5HRrV+CO2x0ZS/kjCx45iJRvAgc9UjngtBBbCHveLDRTpjNPTzGX1JcGdmo0FXcIcBUFX40Dz4ifhos57nU93J74JAbB6gB/O3QNvSQAy9CVQm4K4VTuNsksoMEJOfPRhtNCBKnjbv5ilzhYhApjfRG1LWM1j15BdxvTOsqIY/48HUSZWDxIXS4PJ2WfJ4KLc6j75Su+/p9LesC/8Kfl+NmHm+LWmeEMHha42/SlXLyW/Wkq1cn1yhTJv3K6YaklXJkhAgMBAAECggEAbzOC7EdNQ+wsL5myNfw1+dNwOioxnOveqI7I21Biyup/PqBSNu7gq2/HIpiHwfJt4b/9UjHXTCYZ4rflOZv6yx3bYvWZ/ThApAZVDu9UXakWzErklA1F9uW4seIR/xtstpPjswik+xZCR75mM+4XxXzTM2G/RmuoNBRBtyunqxyv3MFC6XjXj4LjFYJiQOqWhLhyPNpTH2asqoeZeFhf7r++78CVjCgZ3eDaOC7qGHF7ncmdM6ASffKyLFrtz61qbfQ8bYmp68p066EMxq0GWyI0Mbnb1UhKqRi+es231jeuqaxP8Tfzpiyhq+hBB8UjIBvJT9Me1LuR2ncdZ1kjNQKBgQDsfsJIvqWJwlv/leJ/R+jVAiYbzvTIL9Ynhv2xkqya7o8kabV7rcU4HGSI8Uiqkmf/JgY6Sx2G5aJg0/TMwEOaYzG2+ELjrISKls0VRjL7iLO4jqs/RqSXoimrI7RYnzaOF+IyKojNcUP4g+RCOq39Cw+iz9AJRF3jzXD6Je6d3wKBgQDCb55D0jQfTjwh8rQ3pTZeU9daBlzjp+Ja2/mxPQEy9kl51/5KL7o+vO+FObBKV8Fr9wV6Ugu7pM50OP4N9pCEUEsWwD4u3b85ttOutZJEV+nFSjhSfXz8uCvHoOHWB/24uzDK1FVollEl1xflp8qGCt7vN1AVyAiIQY1j102o/wKBgQCa0oEuTmDgxrSjpeQZn2H10OpR2jK1u1tflSgQUA1fgD2FgQ6mYSRzj2Lho9QGlsIubx40m2hBH6Ql75ybTmLzd2zcc7qWgiJi+EZaXxKRSPUfJYdGbWDjQ+4zc7RlMBXvAAO9A8t7c2MkFnQgR3oP5mXeGF4BMxikQaJsoNiN0QKBgFarZHO81kkCGaHUSurm9QY0/zB815kO88ImffW4SLmLuRLb6djtz2o509ah8WgiF/DlSobM3bgF7ZU29mAJAUBJfj8k8FL5u0yhp1ggTZOzUFwGCncSwszv12wD1ccPBtT6c/Rlk7CsGH3ylA9lrMv1OoyNLmCXKt55wem4RkDHAoGAX/XPoTLlWvFo2ZT2lx9yQ9ht8RlqTRDodG249Fu44Ep+QG6pjIqeDBFiO0qxw7FBlFjgXGC2KOQ+X5Ui27gpD3FLBsh9c0H8yr2CZp/ANVTz8ZTGhyb66FwtDIEyAjW6wHRkujxDFNtFrXrgW9SY1r3FSit65V+ygm8hQonTkrE=";
    // 支付宝公钥
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArMxYiRY8OBlxQp7G6tv3f12ofCqWK3Lz+ZH5HdP7N9G+ppMnfChLrcc6u4GFdbgFin9iRziIQgwFUuRtfczYdtpixPrFRLoU+Zl6I45st8hFS4YEGM7ns61Bz8q6qcTta9i9eB+QO57UfqfzonOz72VBuOr4+5NkBRQxuLJVnYdKNPwpdwZ6s9cfUpXhDHH0AMrbAGUpRq8jLMIu9gup85oBTSNxPJG3O8e1/UKpjJeiX0j9gLXEsnU58Uqh4dErJLJacz8nGpZio5gHxNl2SAYsltVvlop9j8dbRvY4y/PaWm6bnuUsxCNZGzfNhj9gbocfaxqSxZCxaSQtVWSoIwIDAQAB";
    //a8SXufhSxDTKporieTq9NO7yDZSpDlAX1zVPT/nf0KWAlxq1TYappWMIYtyrOABhJyn6flNP6vuSBiM5lYsepHvYrtRHqlFiJruEkiaCgEZBKL5aCfBHYj0oqgQn9MpNV/PEH4cBYAVaiI4+VX8CBUQfeEGjgN6OkpLULZ3X0JUkmSnVvCNJ1m3PD68IIlbOfEZXJUKCqmZhzprGR5VWswjxA+g87cMwvijL4gdkSy/daG62Bz5vApcmmMkuX1k1fMWP4ajZCASVw8HD+MSLRhd8We9F97gd8CW0TavzbdR+mTS5H4yEgO8F9HRAsbkhV9yu0yQIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    //http://3761c948r0.wicp.vip
    private  String notify_url = "http://39.108.110.147:4010/bill/asyncMethod";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private  String return_url = "http://39.108.110.147:4010/bill/syncMethod";

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        //System.out.println("支付宝的响应："+result);

        return result;

    }
}
