package com.parkLot.spring.service.impl;

import com.mingke.Domain.LicenseEntity;
import com.mingke.Domain.ParkPointEntity;
import com.mingke.Utils.PageUtils;
import com.mingke.Utils.Query;
import com.parkLot.spring.dao.LicenseDao;
import com.parkLot.spring.service.LicenseService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.util.StringUtils;


@Service("licenseService")
public class LicenseServiceImpl extends ServiceImpl<LicenseDao, LicenseEntity> implements LicenseService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        //LicenseEntity
        String key = (String) params.get("key");
        QueryWrapper<LicenseEntity> wrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(key)) {
            wrapper.like("license_number", key);
        }
        IPage<LicenseEntity> page = this.page(
                new Query<LicenseEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

}