package com.parkLot.spring.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mingke.Domain.ParkEntity;
import com.mingke.TransformObject.LocationTO;
import com.mingke.Utils.PageUtils;


import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:40
 */
public interface ParkService extends IService<ParkEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<ParkEntity> getUserLocation(LocationTO location);
}

