package com.mingke.ViewObject;

import lombok.Data;

@Data
public class PayInfoViewObject {

    //订单编号
    private String out_trade_no;
    // 订单名称，必填
    private String subject;
    // 付款金额，必填
    private String total_amount;
    // 商品描述，可空
    private String body;
    // 超时时间 可空
    private String timeout_express="2m";
    // 销售产品码 必填
    private String product_code="QUICK_WAP_WAY";
}
