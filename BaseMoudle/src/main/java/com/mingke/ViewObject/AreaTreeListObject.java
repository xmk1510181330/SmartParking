package com.mingke.ViewObject;

import com.mingke.Domain.AreaEntity;
import lombok.Data;

import java.util.List;

@Data
public class AreaTreeListObject {

    private AreaEntity selfArea;

    private List<AreaTreeListObject> leafList;

    private Integer isLeaf;
}
