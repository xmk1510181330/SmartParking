package com.mingke.ViewObject;

import lombok.Data;

import java.util.Date;

@Data
public class ParkPointViewObject {

    private Integer customerId;
    private Integer parkPointId;
    /**
     * 节点所属停车场id
     */
    private Integer parkId;
    /**
     * 节点硬件地址
     */
    private String parkPointAddress;
    /**
     * 节点类型
     */
    private Integer parkPointType;
    /**
     * 节点状态
     */
    private Integer parkPointStatus;
    /**
     * 加入时间
     */
    private Date addTime;
    /**
     * 更改时间
     */
    private Date updateTime;
    /**
     * 逻辑删除
     */
    private Integer deleted;
}
