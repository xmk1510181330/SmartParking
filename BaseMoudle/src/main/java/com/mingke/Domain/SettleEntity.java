package com.mingke.Domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:39
 */
@Data
@TableName("smartpark_settle")
public class SettleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer settleId;
	/**
	 * 收费标准名称
	 */
	private String settleName;
	/**
	 * 小车小时单价
	 */
	private Double settleSmallUnitPrice;
	/**
	 * 大车小时单价
	 */
	private Double settleBigUnitPrice;
	/**
	 * 月租价格（需要绑定车牌）
	 */
	private Double settleMonthlyPrice;
	/**
	 * 添加时间
	 */
	private Date addTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Integer deleted;

}
