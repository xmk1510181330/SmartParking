package com.mingke.TransformObject;

import lombok.Data;

@Data
public class ValidationCodeTO {

    private String phone;
    private String code;
}
