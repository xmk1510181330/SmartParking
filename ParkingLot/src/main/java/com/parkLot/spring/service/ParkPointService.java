package com.parkLot.spring.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mingke.Domain.ParkPointEntity;
import com.mingke.TransformObject.PointSwitchTO;
import com.mingke.Utils.PageUtils;
import com.mingke.ViewObject.ParkPointViewObject;


import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:40
 */
public interface ParkPointService extends IService<ParkPointEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<ParkPointViewObject> getPointByParkLotId(Integer pid);

    boolean savePoint(ParkPointEntity parkPoint);

    boolean updatePoint(ParkPointEntity parkPoint);

    boolean switchPointUseStatu(PointSwitchTO to);

    boolean startPoint(Integer id);

    boolean stopPoint(Integer id);

    boolean queryPoint(Integer id);

    boolean errorPoint(Integer id);

    boolean getPointByAddress(String address);
}

