package com.mingke.Domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:40
 */
@Data
@TableName("smartpark_park")
public class ParkEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer parkId;
	/**
	 * 所在区域id
	 */
	private Integer parkAreaId;
	/**
	 * 管理员id
	 */
	private Integer parkAdminId;
	/**
	 * 收费标准id
	 */
	private Integer parkSettleId;
	/**
	 * 停车场名称
	 */
	private String parkName;
	/**
	 * 详细地址
	 */
	private String parkAddress;
	/**
	 * 停车场展示图
	 */
	private String parkPic;
	/**
	 * 停车场总车位数量
	 */
	private Integer parkTotle;
	/**
	 * 已用车位数量
	 */
	private Integer parkInUse;
	/**
	 * 坐标经度
	 */
	private Double parkLatitude;
	/**
	 * 坐标纬度
	 */
	private Double parkLongitude;
	/**
	 * 添加时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Integer daleted;

}
