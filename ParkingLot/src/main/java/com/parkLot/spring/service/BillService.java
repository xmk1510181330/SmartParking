package com.parkLot.spring.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.mingke.Domain.BillEntity;
import com.mingke.Utils.PageUtils;
import com.mingke.ViewObject.BillInfoViewObject;


import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-02-16 11:28:40
 */
public interface BillService extends IService<BillEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<BillInfoViewObject> getBillListByUid(Integer id);
}

