package com.parkLot.spring.controller;

import com.mingke.Domain.AreaEntity;
import com.mingke.Utils.PageUtils;
import com.mingke.Utils.R;
import com.mingke.ViewObject.AreaTreeListObject;

import com.parkLot.spring.service.AreaService;
import com.parkLot.spring.third.MessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:40
 */
@RestController
@RequestMapping("/area")
public class AreaController {

    @Autowired
    private AreaService areaService;

    @Autowired
    private MessageUtils messageUtils;
    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = areaService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{areaId}")
    public R info(@PathVariable("areaId") Integer areaId){
		AreaEntity area = areaService.getById(areaId);

        return R.ok().put("area", area);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody AreaEntity area){
        area.setAddTime(new Date());
        area.setUpdateTime(new Date());
        area.setDeleted(0);
		areaService.save(area);

        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody AreaEntity area){
        area.setUpdateTime(new Date());
		areaService.updateById(area);

        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Integer[] areaIds){
		areaService.removeByIds(Arrays.asList(areaIds));

        return R.ok();
    }

    @GetMapping("/testMessage")
    public R testMessage() throws IOException {

        return messageUtils.sendMessageByPost("13663751068", "3der42");
    }

}
