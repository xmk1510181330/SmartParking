package com.mingke.Domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:40
 */
@Data
@TableName("smartpark_area")
public class AreaEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer areaId;
	/**
	 * 区域父标签id
	 */
	private Integer areaPid;
	/**
	 * 区域名称
	 */
	private String areaName;
	/**
	 * 区域类型
	 */
	private Integer areaCtype;
	/**
	 * 加入时间
	 */
	private Date addTime;
	/**
	 * 更改时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Integer deleted;

}
