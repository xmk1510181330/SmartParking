package com.mingke.ViewObject;


import lombok.Data;

import java.util.Date;

@Data
public class ParkInfoViewObject {

    private Integer parkId;

    private String parkArea;

    private String parkAdmin;

    private Integer parkSettleId;

    private String parkName;

    private String parkAddress;

    private String parkPic;

    private Integer parkTotle;

    private Integer parkInUse;

    private Double parkLatitude;

    private Double parkLongitude;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 逻辑删除
     */
    private Integer daleted;
}
