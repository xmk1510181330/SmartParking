package com.parkLot.spring.service.impl;

import com.mingke.Domain.ParkEntity;
import com.mingke.TransformObject.LocationTO;
import com.mingke.Utils.PageUtils;
import com.mingke.Utils.Query;
import com.mingke.ViewObject.ParkInfoViewObject;
import com.parkLot.spring.dao.ParkDao;
import com.parkLot.spring.service.ParkService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.util.StringUtils;


@Service("parkService")
public class ParkServiceImpl extends ServiceImpl<ParkDao, ParkEntity> implements ParkService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        QueryWrapper<ParkEntity> wrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(key)) {
            wrapper.like("park_name", key);
        }
        IPage<ParkEntity> page = this.page(
                new Query<ParkEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<ParkEntity> getUserLocation(LocationTO location) {
        //判断和用户的定位相近的停车场的信息
        //TODO 查询地理位置进行判断
        QueryWrapper<ParkEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("daleted", 0);
        List<ParkEntity> list = list(wrapper);
        List<ParkInfoViewObject> infos = new ArrayList<>();
        for(ParkEntity entity : list){

        }
        return list;
    }


}