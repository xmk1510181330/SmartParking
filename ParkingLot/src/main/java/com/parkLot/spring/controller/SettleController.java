package com.parkLot.spring.controller;

import com.mingke.Domain.SettleEntity;
import com.mingke.Utils.PageUtils;
import com.mingke.Utils.R;
import com.parkLot.spring.service.SettleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;


/**
 * 
 *
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:39
 */
@RestController
@RequestMapping("/settle")
public class SettleController {

    @Autowired
    private SettleService settleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = settleService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{settleId}")
    public R info(@PathVariable("settleId") Integer settleId){
		SettleEntity settle = settleService.getById(settleId);

        return R.ok().put("settle", settle);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody SettleEntity settle){
        settle.setUpdateTime(new Date());
        settle.setAddTime(new Date());
        settle.setDeleted(0);
		settleService.save(settle);

        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody SettleEntity settle){
        settle.setUpdateTime(new Date());
		settleService.updateById(settle);

        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Integer[] settleIds){
		settleService.removeByIds(Arrays.asList(settleIds));

        return R.ok();
    }

}
