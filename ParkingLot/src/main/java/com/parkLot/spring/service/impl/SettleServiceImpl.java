package com.parkLot.spring.service.impl;

import com.mingke.Domain.SettleEntity;
import com.mingke.Utils.PageUtils;
import com.mingke.Utils.Query;
import com.parkLot.spring.dao.SettleDao;
import com.parkLot.spring.service.SettleService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.util.StringUtils;


@Service("settleService")
public class SettleServiceImpl extends ServiceImpl<SettleDao, SettleEntity> implements SettleService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        QueryWrapper<SettleEntity> wrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(key)) {
            wrapper.like("settle_name", key);
        }
        IPage<SettleEntity> page = this.page(
                new Query<SettleEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

}