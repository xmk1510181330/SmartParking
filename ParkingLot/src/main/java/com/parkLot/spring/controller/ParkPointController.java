package com.parkLot.spring.controller;

import com.mingke.Domain.ParkPointEntity;
import com.mingke.TransformObject.PointSwitchTO;
import com.mingke.Utils.PageUtils;
import com.mingke.Utils.R;
import com.mingke.ViewObject.ParkPointViewObject;
import com.parkLot.spring.service.ParkPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:40
 * 规定一下节点的状态
 * 101 -- 节点未启用
 * 102 -- 节点使用中
 * 103 -- 节点掉线无信号
 *
 * 规定一下节点类型
 * 1 -- 中心节点，一个停车场一个
 * 2 -- 工作节点，一个停车场多个
 * 组成星状网络
 */
@RestController
@RequestMapping("/parkpoint")
public class ParkPointController {

    @Autowired
    private ParkPointService parkPointService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = parkPointService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{parkPointId}")
    public R info(@PathVariable("parkPointId") Integer parkPointId){
		ParkPointEntity parkPoint = parkPointService.getById(parkPointId);

        return R.ok().put("parkPoint", parkPoint);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody ParkPointEntity parkPoint){
        parkPoint.setAddTime(new Date());
        parkPoint.setUpdateTime(new Date());
        parkPoint.setDeleted(0);
		//parkPointService.save(parkPoint);
		boolean result = parkPointService.savePoint(parkPoint);
		if(result){
		    return R.ok();
        }else {
		    return R.error();
        }
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody ParkPointEntity parkPoint){
        parkPoint.setUpdateTime(new Date());
		//parkPointService.updateById(parkPoint);
        boolean result = parkPointService.updatePoint(parkPoint);

        if(result){
            return R.ok();
        }else {
            return R.error();
        }
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Integer[] parkPointIds) {
        parkPointService.removeByIds(Arrays.asList(parkPointIds));

        return R.ok();
    }

    @GetMapping("/getPointByParkLotId")
    public R getPointByParkLotId(@RequestParam("pid") Integer pid){
        List<ParkPointViewObject> result = parkPointService.getPointByParkLotId(pid);
        return R.ok().put("result", result);
    }

    @PostMapping("switchPointUseStatu")
    public R switchPointUseStatu(@RequestBody PointSwitchTO to){
        boolean result = parkPointService.switchPointUseStatu(to);

        if(result){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @GetMapping("startPoint")
    public R startPoint(@RequestParam("id") Integer id){
        if(id != 0){
            boolean result = parkPointService.startPoint(id);
            if(result){
                return R.ok("操作成功");
            }else{
                return R.error("操作失败");
            }
        }else{
            return  R.error("参数不全");
        }
    }

    @GetMapping("stopPoint")
    public R stopPoint(@RequestParam("id") Integer id){
        if(id != 0){
            boolean result = parkPointService.stopPoint(id);
            if(result){
                return R.ok("操作成功");
            }else{
                return R.error("操作失败");
            }
        }else{
            return  R.error("参数不全");
        }
    }

    @GetMapping("queryPoint")
    public R queryPoint(@RequestParam("id") Integer id){
        if(id != 0){
            boolean result = parkPointService.queryPoint(id);
            if(result){
                return R.ok("操作成功");
            }else{
                return R.error("操作失败");
            }
        }else{
            return  R.error("参数不全");
        }
    }

    @GetMapping("errorPoint")
    public R errorPoint(@RequestParam("id") Integer id){
        if(id != 0){
            boolean result = parkPointService.errorPoint(id);
            if(result){
                return R.ok("操作成功");
            }else{
                return R.error("操作失败");
            }
        }else{
            return  R.error("参数不全");
        }
    }

}
