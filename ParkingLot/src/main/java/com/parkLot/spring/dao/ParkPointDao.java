package com.parkLot.spring.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mingke.Domain.ParkPointEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:40
 */
@Mapper
public interface ParkPointDao extends BaseMapper<ParkPointEntity> {
	
}
