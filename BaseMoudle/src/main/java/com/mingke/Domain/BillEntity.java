package com.mingke.Domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *
 *
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-02-16 11:28:40
 */
@Data
@TableName("smartpark_bill")
public class BillEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer billId;
	/**
	 * 用户id
	 */
	private Integer billCustomerId;
	/**
	 * 牌照id
	 */
	private Integer billLicenseId;
	/**
	 * 停车场id
	 */
	private Integer billParkingId;
	/**
	 * 账单总计费用
	 */
	private Double billFee;
	/**
	 * 账单状态
	 */
	private Integer billStatu;
	/**
	 * 开始时间
	 */
	private Date billStart;
	/**
	 * 结束时间
	 */
	private Date billEnd;
	/**
	 * 添加时间
	 */
	private Date addTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Integer deleted;

}