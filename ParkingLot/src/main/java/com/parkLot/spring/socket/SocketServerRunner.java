package com.parkLot.spring.socket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/*
 * 使得SocketServer伴随项目启动的配置类
 */
@Component
public class SocketServerRunner implements CommandLineRunner {

    @Autowired
    private SocketServerPointHex socketServer;

    @Override
    public void run(String... args) throws Exception {
        socketServer.startSocket();
    }
}
