package com.parkLot.spring.service.impl;

import com.mingke.Domain.CustomerEntity;
import com.mingke.Domain.LicenseEntity;
import com.mingke.Utils.PageUtils;
import com.mingke.Utils.Query;
import com.parkLot.spring.dao.CustomerDao;
import com.parkLot.spring.service.CustomerService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.util.StringUtils;


@Service("customerService")
public class CustomerServiceImpl extends ServiceImpl<CustomerDao, CustomerEntity> implements CustomerService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        //CustomerEntity
        String key = (String) params.get("key");
        QueryWrapper<CustomerEntity> wrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(key)) {
            wrapper.like("customer_realname", key);
        }
        IPage<CustomerEntity> page = this.page(
                new Query<CustomerEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

}