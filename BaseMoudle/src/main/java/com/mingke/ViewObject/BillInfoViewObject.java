package com.mingke.ViewObject;

/*
 * 要实现的内容
 * 1. 计算停车时间，例如：1天2小时34分钟
 * 2. 根据停车场的收费标准计算费用
 * 3. 封装停车场信息
 */

import lombok.Data;

import java.util.Date;

@Data
public class BillInfoViewObject {

    private Integer billId;
    /**
     * 用户id
     */
    private Integer billCustomerId;
    /**
     * 牌照id
     */
    private Integer billLicenseId;
    /**
     * 停车场id
     */
    private Integer billParkingId;
    private String billParkName;
    /**
     * 账单总计费用
     */
    private Double billFee;
    /**
     * 账单状态
     */
    private Integer billStatu;
    /**
     * 开始时间
     */
    private Date billStart;
    /**
     * 结束时间
     */
    private Date billEnd;
    private String totleTime;
    /**
     * 添加时间
     */
    private Date addTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 逻辑删除
     */
    private Integer deleted;
}
