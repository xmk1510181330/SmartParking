package com.mingke.Domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:40
 */
@Data
@TableName("smartpark_license")
public class LicenseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer licenseId;
	/**
	 * 用户id
	 */
	private Integer licenseCustomerId;
	/**
	 * 车牌号码
	 */
	private String licenseNumber;
	/**
	 * 类型（是否为月租车辆）
	 */
	private Integer licenseType;
	/**
	 * 添加时间
	 */
	private Date addTime;
	/**
	 * 更改时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Integer deleted;

}
