package com.mingke.TransformObject;


public class LocationTO {

    private String administrativeArea;
    private String locality;
    private String subLocality;
    private String thoroughfare;
    private String name;

    public String getAdministrativeArea() {
        return administrativeArea;
    }

    public void setAdministrativeArea(String administrativeArea) {
        this.administrativeArea = administrativeArea;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getSubLocality() {
        return subLocality;
    }

    public void setSubLocality(String subLocality) {
        this.subLocality = subLocality;
    }

    public String getThoroughfare() {
        return thoroughfare;
    }

    public void setThoroughfare(String thoroughfare) {
        this.thoroughfare = thoroughfare;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationTO() {
        super();
    }

    public LocationTO(String administrativeArea, String locality, String subLocality, String thoroughfare, String name) {
        this.administrativeArea = administrativeArea;
        this.locality = locality;
        this.subLocality = subLocality;
        this.thoroughfare = thoroughfare;
        this.name = name;
    }

    @Override
    public String toString() {
        return "LocationTO{" +
                "administrativeArea='" + administrativeArea + '\'' +
                ", locality='" + locality + '\'' +
                ", subLocality='" + subLocality + '\'' +
                ", thoroughfare='" + thoroughfare + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
