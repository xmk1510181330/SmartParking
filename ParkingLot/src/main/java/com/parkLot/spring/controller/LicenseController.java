package com.parkLot.spring.controller;

import com.mingke.Domain.LicenseEntity;
import com.mingke.Utils.PageUtils;
import com.mingke.Utils.R;
import com.parkLot.spring.service.LicenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;


/**
 * 
 *
 * @author mingke
 * @email 1510181330@qq.com
 * @date 2021-01-22 13:30:40
 */
@RestController
@RequestMapping("/license")
public class LicenseController {

    @Autowired
    private LicenseService licenseService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = licenseService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{licenseId}")
    public R info(@PathVariable("licenseId") Integer licenseId){
		LicenseEntity license = licenseService.getById(licenseId);

        return R.ok().put("license", license);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody LicenseEntity license){
        license.setAddTime(new Date());
        license.setUpdateTime(new Date());
        license.setDeleted(0);
		licenseService.save(license);

        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody LicenseEntity license){
        license.setUpdateTime(new Date());
		licenseService.updateById(license);

        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Integer[] licenseIds){
		licenseService.removeByIds(Arrays.asList(licenseIds));

        return R.ok();
    }

}
